import * as THREE from 'three';
let OrbitControls = require('three-orbit-controls')(THREE)
// console.log(THREE);
// OrbitControls === undefined  // false

//declare variables
let scene, camera, renderer, geometry, bodyMesh, clips, controls;

let mixer = new THREE.AnimationMixer();
let clock = new THREE.Clock();




//initialising the scene
function initScene() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera( 60, window.innerWidth/window.innerHeight, 0.1, 1000 );
  renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );
  camera.position.z = 5;

  controls = new OrbitControls(camera);
    controls.target = new THREE.Vector3(0,0,0);
  controls.update();
};

//multi direction lights
function initLighting() {
  var light = new THREE.DirectionalLight( 0xffffff, 1 );
  light.position.set( 0, 1, 0 );
  scene.add( light );

  var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
  light.position.set( 0, -1, 0 );
  scene.add( light );

  var light = new THREE.DirectionalLight( 0xffffff, 1 );
  light.position.set( 1, 0, 0 );
  scene.add( light );

  var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
  light.position.set( 0, 0, 1 );
  scene.add( light );

  var light = new THREE.DirectionalLight( 0xffffff, 1 );
  light.position.set( 0, 0, -1 );
  scene.add( light );

  var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
  light.position.set( -1, 0, 0 );
  scene.add( light );
}

////////////////////////////
// MESHES /////////////////
//////////////////////////
let loader = new THREE.JSONLoader();
loader.load(
    "dummy.json", 
    function (geometry, materials){
        let material = materials[0];
        let bodyMesh = new THREE.SkinnedMesh(geometry, material);
        bodyMesh.position.set(0, -2, 0);
        bodyMesh.scale.set(2,2,2);

        scene.add(bodyMesh);
        clips = geometry.animations;
        mixer.clipAction(clips[1], bodyMesh).play();

        // clips.forEach(function(clip){
        //     mixer.clipAction(clip).play();
        // });
    },
    
//on progress callback
    function (xhr){
        console.log((xhr.loaded/xhr.total*100) + "% loaded...");
    },
//on error callback
    function (err){
        console.log("An error occured. Stop writing crap code.");
    }
);

//old mesh caller for integrated basic models
// function initGeometry() {
//   bodyMesh = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors } ));
//   scene.add( bodyMesh );
// }



//render the scene
function render() {
  requestAnimationFrame( render );
  mixer.update(clock.getDelta);

  controls.update();

  renderer.render(scene, camera);
};

//on resize of the screen
function resize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}

/////////////////////////////////////////////
// ANIMATION ///////////////////////////////
///////////////////////////////////////////
let bodyMixer = new THREE.AnimationMixer(bodyMesh);


//play all animations


//run
initScene();
initLighting();


render();

window.addEventListener("resize", resize);
